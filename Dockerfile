FROM node:11 as nodeBase

ENV PORT=6868 SUBSCRIBE_ADDRESS="tcp://172.31.224.158:33987"

WORKDIR /app

COPY . /app

RUN npm install

ENV TINI_VERSION v0.19.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--"]

CMD npm start

EXPOSE 6868

const zmq = require("zeromq")
const { ZMQ_TCP_KEEPALIVE, ZMQ_TCP_KEEPALIVE_IDLE, ZMQ_TCP_KEEPALIVE_INTVL } = require("zeromq")
const dayjs = require("dayjs")
const express = require("express")
const expressWs = require("express-ws")
const path = require("path")

const SUBSCRIBE_ADDRESS = "tcp://0.0.0.0:33987"
const SUBSCRIBE_CHANNEL = "zmq-pub-test"
const EXPRESS_PORT = 6868

const subAddress = process.env.SUBSCRIBE_ADDRESS || SUBSCRIBE_ADDRESS
const subChannel = process.env.SUBSCRIBE_CHANNEL || SUBSCRIBE_CHANNEL
const expressPort = process.env.PORT || EXPRESS_PORT

const wsServer = expressWs(express())
const app = wsServer.app
const aWss = wsServer.getWss("/")

app.use("/showcase", express.static(path.resolve(__dirname, "./public")))
app.ws("/", (ws, req) => {
  console.log("Socket connected")
})
app.listen(expressPort, () => {
  console.log(`Web server & WebSocket is listening on port ${expressPort}`)
})

const socket = zmq.socket("sub")

socket.setsockopt(ZMQ_TCP_KEEPALIVE, 1)
socket.setsockopt(ZMQ_TCP_KEEPALIVE_IDLE, 1)
socket.setsockopt(ZMQ_TCP_KEEPALIVE_INTVL, 1)

socket.connect(subAddress)
socket.subscribe(subChannel)

console.log(`Subscribed ${subAddress} with topic ${subChannel}`);

socket.on("message", (topic, message) => {
  const payload = `${dayjs().format("YYYY-MM-DD HH:mm:ss.SSS")} Received message from ${topic}: ${JSON.parse(message)}`

  console.log(payload)

  aWss.clients.forEach((client) => {
    client.send(payload)
  })
})
